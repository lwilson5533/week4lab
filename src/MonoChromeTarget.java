import javafx.scene.paint.Color;
import wheelsFX.users.Frame.Frame;

/**
 * MonoChromeTarget.java:
 *
 * Displays a simple archery target using multiple Wheels Shapes.
 * The entire target is built in a call to the super class method, makeTarget
 * then each levels color is altered to create a monochrome target in the
 * overridden method, markTarget
 * This class extends the Target class
 *
 * makeTarget has position arguments.
 *
 * @author Laura Wilson
 */

public class MonoChromeTarget extends Target
{

    /**
     * constructor for the MonoChromeTarget class
     */
    public MonoChromeTarget ()
    {
        super();
    }

    /**
     * constructor for the MonoChromeTarget class using some:
     *
     * @param x
     * @param y
     */

    public MonoChromeTarget (int x, int y)
    {
        super(x,y);
    }

    /**
     * overridden method makeTarget, calls super class method makeTarget,
     * then alters the color of each level to create a monochrome effect.
     *
     * @param x int
     * @param y int
     */

    public void makeTarget(int x, int y)
    {
        super.makeTarget(x,y);

        level1.setColor(Color.BLACK);

        level2.setColor(Color.WHITE);

        level3.setColor(Color.BLACK);

        level4.setColor(Color.WHITE);

    }

    /**
     * main method to demonstrate the MonoChromeTarget class
     *
     * @param args
     */

    public static void main (String[] args)
    {
        MonoChromeTarget t1 = new MonoChromeTarget(50,10);
        MonoChromeTarget t2 = new MonoChromeTarget();

        t1.move(200,100);
        t2.setLocation(300,400);

        Frame.createFrame();
    }
}
