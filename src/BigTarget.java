import javafx.scene.paint.Color;
import wheelsFX.users.Frame.Frame;
import wheelsFX.users.Shapes.Ellipse;


/**
 * BigTarget.java:
 *
 * Displays a simple archery target using multiple Wheels Shapes.
 *
 * This class extends the Target class.
 * The entire target is built using a call to the method makeTarget in
 * the superclass and using the overridden method, makeTarget.
 * The BigTarget includes large levels 1-4 and 2 additional
 * levels, 5 and 6.
 *
 * makeTarget has position arguments.
 *
 * @author Laura Wilson
 */

public class BigTarget extends Target
{
    private int upLevel1Size = level1Size*2;
    private int upLevel2Size = level2Size*2;
    private int upLevel3Size = level3Size*2;
    private int upLevel4Size = level4Size*2;
    private int Level5Size = 20;
    private int Level6Size = 5;

    private Ellipse Level5;
    private Ellipse Level6;

    /**
     * Constructor for the BigTarget class
     */
    public BigTarget()
    {
        super();
        this.makeTarget(0,0);
    }

    /**
     * constructor for the BigTarget class at some:
     *
     * @param x
     * @param y
     */
    public BigTarget(int x, int y)
    {
        super();
        this.makeTarget(x,y);
    }

    /**
     * overriding makeTarget method from Target class
     * this method with build a target at some:
     *
     * @param x int
     * @param y int
     */
    public void makeTarget (int x, int y)
    {
        super.makeTarget(x,y);

        level1.setSize(upLevel1Size, upLevel1Size);

        level2.setSize(upLevel2Size, upLevel2Size);

        level3.setSize(upLevel3Size, upLevel3Size);

        level4.setSize(upLevel4Size, upLevel4Size);

        Level5 = new Ellipse(x, y);
        Level5.setSize(Level5Size, Level5Size);
        Level5.setColor(Color.GRAY);
        super.add(this.Level5);

        Level6 = new Ellipse(x, y);
        Level6.setSize(Level6Size, Level6Size);
        Level6.setColor(Color.WHITE);
        super.add(this.Level6);
    }

    /**
     * main method to demonstrate BigTarget class
     *
     * @param args
     */
    public static void main (String[] args)
    {
        BigTarget t1 = new BigTarget();

        t1.setLocation(200,200);
        t1.move(200,200);

        Frame.createFrame();
    }
}
