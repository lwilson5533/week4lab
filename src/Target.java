import javafx.scene.paint.Color;
import wheelsFX.users.Frame.Frame;
import wheelsFX.users.Shapes.Ellipse;
import wheelsFX.users.Shapes.ShapeGroup;

/**
 * TargetApp.java:
 *
 * Displays a simple archery target using multiple Wheels Shapes.
 * The entire target is built in a method, makeTarget.
 * This class extends the ShapeGroup class of wheelsFx.
 *
 * makeTarget has position arguments.
 *
 * @author Laura Wilson
 * @author Professor Rossi
 */

public class Target extends ShapeGroup
{
    //---------------- instance variables ------------------------------
    // local "constant" variables define the sizes of all circles
    protected final int level1Size = 80;
    protected final int level2Size = 50;
    protected final int level3Size = 30;
    protected final int level4Size = 20;

    // other local variables are used to references Wheels objects
    // used to draw the target.
    protected Ellipse level1;
    protected Ellipse level2;
    protected Ellipse level3;
    protected Ellipse level4;
    // -----------------------------------------------------------------

    /**
     * Constructor for the Target class.
     */
    public Target()
    {
        super();
        this.makeTarget(0, 0);
    }

    /**
     * 2 parameter constructor goes here:
     */

    public Target(int x, int y)
    {
        super();
        this.makeTarget(x, y);
    }



    /**
     * move( int dx, int dy ).
     * move the location of the target by dx and dy
     * newx = oldx + dx
     * newy = oldy + dy
     * use Target's setLocation method to actually change the location
     *
     * @param dx int
     * @param dy int
     */
    public void move(int dx, int dy)
    {
        super.setLocation(super.getXLocation()+dx,super.getYLocation()+dy);
    }

    // -----------------------------------------------------------------

    /**
     * makeTarget.
     * encapsulates all the Wheels components needed to draw a target.
     *
     * @param x int
     * @param y int
     */
    public void makeTarget(int x, int y)
    {
        // create the level1 circle
        level1 = new Ellipse(x, y);
        level1.setSize(level1Size, level1Size);
        super.add(this.level1);

        // create the next level4 circle
        level2 = new Ellipse(x, y);
        level2.setSize(level2Size, level2Size);
        level2.setColor(Color.BLUE);
        super.add(this.level2);

        // create the next level4 circle
        level3 = new Ellipse(x, y);
        level3.setSize(level3Size, level3Size);
        level3.setColor(Color.CYAN);
        super.add(this.level3);

        // create the level4 circle
        level4 = new Ellipse(x, y);
        level4.setColor(Color.BLACK);
        level4.setSize(level4Size, level4Size);
        super.add(this.level4);
    }

    // -----------------------------------------------------------------

    /**
     * main program just invokes the class constructor.
     *
     * @param args String
     */
   public static void main (String[] args)
   {
       Target t1 = new Target();

       Frame.createFrame();
   }
}


